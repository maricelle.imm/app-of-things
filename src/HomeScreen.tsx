import React from "react";
import { View, Text } from "react-native";

export default function HomeScreen() {
  return (
    <View
      style={[
        {
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
        },
      ]}
    >
      <Text style={{ fontSize: 28 }}>Hello, user!</Text>
      <Text style={{ fontSize: 14, marginVertical: 8 }}>
        This is the home page
      </Text>
    </View>
  );
}
